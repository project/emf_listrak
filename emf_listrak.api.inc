<?php
// $Id$

/**
 * @file
 * Listrak API call wrappers
 *
 * @author Tom Kirkpatrick (mrfelton), www.systemseed.com
 */

/**
 * Subscribe a user to a list.
 *
 * @param $email
 *   String; E-mail address to subscribe
 * @param $fields
 *   Array; Array of custom field values. Key is field. Value is value for the field.
 * @param $lid
 *   String; List ID of the list to subscribe to.
 * @return
 *   Boolean; TRUE if user is subscribed. FALSE if not.
 */
function emf_listrak_api_subscribe($email, $fields, $lid) {
  return listrak_subscribe_with_profile_data($email, $fields, $lid);
}

/**
 * Unsubscribe a user from a list.
 *
 * @param $email
 *   String; E-mail address to subscribe
 * @param $lid
 *   String; List ID of the list to subscribe to.
 * @return
 *   Boolean; TRUE if user is subscribed. FALSE if not.
 */
function emf_listrak_api_unsubscribe($email, $lid) {
  return listrak_unsubscribe($email, $lid);
}

/**
 * Fetch subscribed subscribers from API.
 *
 * @param $date
 *   Mixed; If a string, should be in the date() format of 'Y-m-d H:i:s', otherwise, a Unix timestamp.
 * @param $lid
 *   String; List ID
 * @return
 *   Array; List of subscriber lists.
 */
function emf_listrak_api_get_subscribers_subscribed($date = 0, $lid = NULL) {
  $subscribers = listrak_get_subscribers($lid, $date);
  return _emf_listrak_api_get_subscribers_format($subscribers);
}

/**
 * Fetch unsubscribed subscribers from API.
 *
 * @param $date
 *   Mixed; If a string, should be in the date() format of 'Y-m-d H:i:s', otherwise, a Unix timestamp.
 * @param $lid
 *   String; List ID
 * @return
 *   Array; List of subscriber lists.
 */
function emf_listrak_api_get_subscribers_unsubscribed($date = 0, $lid = NULL) {
  $unsubscribers = listrak_get_unsubscribers($lid, $date);
  return _emf_listrak_api_get_subscribers_format($unsubscribers);
}

/**
 * Turns a WSContactSubscriptionInfo object into a simple list of emails.
 *
 */
function _emf_listrak_api_get_subscribers_format($subscribers) {
  $data = array();
  if ($subscribers->WSContactSubscriptionInfo) {
    foreach ($subscribers->WSContactSubscriptionInfo as $subscriber) {
      $data[] = $subscriber->EmailAddress;
    }
  }
  return $data;
}

/**
 * Fetch lists from API.
 *
 * @return
 *   Array; List of subscriber lists.
 */
function emf_listrak_api_get_lists() {
  $client = listrak_request();
  $lists = array();
  
  // Do call.
  $response = $client->GetContactListCollection();
  
  // Build up list of lists.
  $result = $response->GetContactListCollectionResult;

  foreach ($result->WSContactList as $row) {
    $list = new stdClass();
    $list->lid = $row->ListID;
    $list->name_api = $row->ListName;
    $list->name = $row->ListName;
    $list->status = TRUE;
    $list->name_action = NULL;
    $list->name_action_long = NULL;
    $list->description = NULL;
    $list->language = NULL;
    $lists[$row->ListID] = $list; 
  };
  
  return $lists;
}

/**
 * Fetch custom fields for some list from API.
 *
 * @param $lid
 *   String; List ID of the list.
 * @return
 *   Array; List of custom fields.
 */
function emf_listrak_api_get_custom_fields($lid) {
  $client = listrak_request();
  $fields = array();

  $params = array(
    'ListID' => $lid,
  );
  
  // Do call
  $response = $client->GetProfileHeaderCollection($params);

  // Build up list of fields.
  $result = $response->GetProfileHeaderCollectionResult;
  
  foreach ($result->WSProfileHeader as $header) {
    
    if (is_array($header->WSProfileAttributes)) {
      foreach ($header->WSProfileAttributes as $sub_header) {
        $field = _emf_listrak_build_field_from_profile_attributes($sub_header);
        $fields[$field->key] = $field;
      }
    }
    else {
      $field = _emf_listrak_build_field_from_profile_attributes($header->WSProfileAttributes);
      $fields[$field->key] = $field;
    }
  }

  return $fields;
}

/**
 * Converts a WSProfileAttributes object into an EMF friendly representation of the list.
 *
 * @return
 *   Object; Object representing a profile attribute.
 */
function _emf_listrak_build_field_from_profile_attributes($attributes) {
  // field type mapping
  $field_type_mapping = array(
    'Text' => 'text',
    'Numeric' => 'text',
    'Date' => 'text',
    'RadioButton' => 'select_one',
    'CheckBox' => 'select_many',
  );
  
  $field = new stdClass();
  $field->key = $attributes->AttributeID;
  $field->name = $attributes->Name;
  $field->type = $field_type_mapping[$attributes->DataType];
  $field->options = NULL;
  
  return $field;
}

/**
 * Convert a UNIX timestamp to a date Listrak wants to receive.
 *
 * @param $timestamp
 *   Integer; The UNIX timestamp to convert.
 * @return
 *   String; The Date in Campaign Monitor format.
 */
function emf_listrak_api_unix_to_service_time($timestamp = 0) {
  if ($timestamp) {
    return date("Y-m-d", $timestamp) . 'T' . date("H:i:s", $timestamp);
  }
  return 0;
}
